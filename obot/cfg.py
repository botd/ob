# OBOT - make the machine ob, not the human.
#
# edit configuration. 

import ob
import obot
import os

def __dir__():
    return ("cfg", "main") 

def cfg(event):
    assert(ob.workdir)
    if not event.args:
        cn = "ob.Cfg"
        target = "main"
    else:
        target = event.args[0]
        cn = "obot.%s.Cfg" % target
    db = ob.dbs.Db()
    l = db.last(cn)
    print(l)
    if not l:     
        l = ob.typ.get_cls(cn)()
        print(type(l))
        d = obot.dft.defaults.get(target, None)
        print(d)
        if d:
            l.update(d)
    if len(event.args) == 1:
        event.reply(l)
        return
    if len(event.args) == 2:
        event.reply(l.get(event.args[1]))
        return
    setter = {event.args[1]: event.args[2]}
    l.edit(setter)
    l.save()
    event.reply("ok")

def main(event):
    event.reply(ob.cfg)
