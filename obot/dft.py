# OB - make the machine ob, not the human.
#
# default values.

import ob

default_irc = {
    "channel": "#obd",
    "nick": "obd",
    "ipv6": False,
    "port": 6667,
    "server": "localhost",
    "ssl": False,
    "realname": "obd",
    "username": "original bad designer"
}

default_main = {
    "workdir": "/var/log/obd",
    "modules": "obot",
    "level": "debug",
}

defaults = ob.Object()
defaults.irc = ob.Object(default_irc)
defaults.main = ob.Object(default_main)
