# OBOT - make the machine ob, not the human.
#
# show status information.

import ob
import obot
import os
import threading
import time

def cfg(event):
    assert(ob.workdir)
    if not event.args:
        files = [x.split(".")[-2].lower() for x in os.listdir(os.path.join(ob.workdir, "store")) if x.endswith("Cfg")]
        if files:
            event.reply("choose from %s" % "|".join(["main",] + list(set(files))))
        else:
            event.reply("no configuration files yet.")
        return
    target = event.args[0]
    if target == "main":
        event.reply(ob.cfg)
        return
    cn = "obot.%s.Cfg" % target
    db = ob.dbs.Db()
    l = db.last(cn)
    if not l:     
        event.reply("no %s found." % cn)
        return
    if len(event.args) == 1:
        event.reply(l)
        return
    if len(event.args) == 2:
        event.reply(l.get(event.args[1]))
        return
    setter = {event.args[1]: event.args[2]}
    l.edit(setter)
    l.save()
    event.reply("ok")

def cmds(event):
    b = ob.plain.by_orig(event.orig)
    event.reply(",".join(sorted(b.cmds)))

def objs(event):
    try:
        index = int(event.args[0])
        event.reply(str(ob.plain.objs[index]))
        return
    except (TypeError, ValueError, IndexError):
        pass
    event.reply([ob.typ.get_type(x) for x in ob.plain.objs])

def ps(event):
    psformat = "%-8s %-50s"
    result = []
    for thr in sorted(threading.enumerate(), key=lambda x: x.getName()):
        if str(thr).startswith("<_"):
            continue
        d = vars(thr)
        o = ob.Object()
        o.update(d)
        if o.get("sleep", None):
            up = o.sleep - int(time.time() - o.state.latest)
        else:
            up = int(time.time() - ob.starttime)
        result.append((up, thr.getName(), o))
    nr = -1
    for up, thrname, o in sorted(result, key=lambda x: x[0]):
        nr += 1
        res = "%s %s" % (nr, psformat % (ob.tms.elapsed(up), thrname[:60]))
        if res.strip():
            event.reply(res)

def up(event):
    event.reply(ob.tms.elapsed(time.time() - ob.starttime))

def v(event):
    event.reply("%s %s" % (ob.cfg.name.upper(), obot.__version__))
