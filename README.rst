R E A D M E
###########

OB - make the machine ob, not the human. no copyright. no  LICENSE.


I N S T A L L 


download with pip3 and install globally.

 > sudo pip3 install ob --upgrade

clone the source:

 > git clone http://bitbucket.org/botd/ob
 > cd ob
 > sudo python3 setup.py install

you can also use the install --user option of setup.py to do a local install.

 > python3 setup.py install ob --user


U S A G E


 > ob v  (runs a command)
 > ob -s (runs a shell)
 > ob -d (daemon mode)
 > ob -m irc -p localhost \#obot obot

you can use the -d option to start the bot in the background and logfiles can be found in ~/.ob/logs.


add url:

 > ob rss https://news.ycombinator.com/rss
 ok 1

 run the rss commad to see what urls are registered:

 > ob rss
 0 https://news.ycombinator.com/rss

using udp to relay text into a channel, start the bot with -m udp and use
the obudp program to send text to the UDP to channel server:

 > tail -f ~/.ob/logs/ob.log | ./bin/oudp 

the default shell user is root@shell and gives access to all the commands that are available.
you can use the --owner option to set the owner of the bot to your own userhost.


C O D I N G


OB contains the following modules:

    ob                          - object library.
    ob.clk                      - clock functions.
    ob.dbs                      - database.
    ob.evt                      - basic event.
    ob.gnr                      - generic object functions.
    ob.hdl                      - handler.
    ob.ldr                      - module loader.
    ob.shl                      - shell code.
    ob.tms                      - time related functions.
    ob.thr                      - threads.
    ob.typ                      - typing.
    obot.cfg                    - config command.
    obot.cmd                    - list of commands.
    obot.fnd                    - find objects.
    obot.irc                    - irc bot.
    obot.rss                    - feed fetcher.
    obot.shw                    - show runtime.
    obot.udp                    - udp to irc relay
    obot.usr                    - user management.

if you want to add your own modules to the bot, you can put you .py files in a "mods" directory and use the -m option to point to that directory.

basic code is a function that gets an event as a argument:

 def command(event):
     << your code here >>

to give feedback to the user use the event.reply(txt) method:

 def command(event):
     event.reply("yooo %s" % event.origin)

to be able to handle the event it needs orig, origin and txt attributes set. 
the orig attribute is a string of the bot's repr, it is used to identify the bot to give the reply to.
one can use the bot's event method to create a basic event to use.

the event most important attributes are:

1) channel - the channel to display the response in.
2) orig - a repr() of the bot this event originated on
3) origin - a userhost of the user who created the event.
4) txt - the text the event is generated with. 

the event.parse() method takes a txt argument to parse the text into an event:

 event = Event()
 event.parse("cmds")
 event.orig = repr(bot)
 event.origin = "root@shell"


have fun coding ;]


you can contact me on IRC/freenode/#dunkbots.

| Bart Thate (bthate@dds.nl, thatebart@gmail.com)
| botfather on #dunkbots irc.freenode.net
