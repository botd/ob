# OB - make the machine ob, not the human. no copyright. no LICENSE.
#
# tests attributes on Objects

import ob
import time
import unittest

class Test_Attribute(unittest.TestCase):

    def timed(self):
        with self.assertRaises((AttributeError, )):
            o = ob.Object()
            o.timed2

    def timed2(self):
        o = ob.Object()
        o.date = time.ctime(time.time())
        self.assert_(o.timed())
