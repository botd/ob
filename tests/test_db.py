# OB - make the machine ob, not the human. no copyright. no LICENSE.
#
# database tests.

import ob
import unittest

class Test_Store(unittest.TestCase):

    def test_emptyargs(self):
        db = ob.dbs.Db()
        res = list(db.find("", {}))
        self.assertEqual(res, [])
