# OB - make the machine ob, not the human. no copyright. no LICENSE.
#
# test load/save

import json
import ob
import os
import unittest

class ENOTCOMPAT(Exception):
    pass

class Test_Core(unittest.TestCase):

    def test_load2(self):
        o = ob.Object()
        o.bla = "mekker"
        p = o.save()
        oo = ob.Object().load(p)
        self.assertEqual(oo.bla, "mekker")

    def test_save(self):
        o = ob.Object()
        p = o.save()
        self.assertTrue(os.path.exists(os.path.join(ob.workdir, "store", p)))

    def test_subitem(self):
        o = ob.Object()
        o.test2 = ob.Object()
        p = o.save()
        oo = ob.Object()
        oo.load(p)
        self.assertTrue(type(oo.test2), ob.Object)

    def test_subitem2(self):
        o = ob.Object()
        o.test = ob.Object()
        o.test.bla = "test"
        p = o.save()
        oo = ob.Object().load(p)
        self.assertTrue(type(oo.test.bla), "test")
