# OB - make the machine ob, not the human. no copyright. no LICENSE.
#
# loader tests.

import ob
import os
import unittest

class Test_Loader(unittest.TestCase):

    def test_loadmod(self):
        l = ob.ldr.Loader()
        l.load_mod("ob.ldr")
        p = l.save()
        ll = ob.ldr.Loader()
        ll.load(p)
        self.assertTrue("ob.ldr" in ll.table)

    def test_loadmod2(self):
        l = ob.ldr.Loader()
        l.load_mod("ob.hdl")
        self.assertTrue("ob.hdl" in l.table)
