# OB - make the machine ob, not the human. no copyright. no LICENSE.
#
# scheduler tests

import ob
import unittest

class Event(ob.evt.Event):

    def show(self):
        if ob.cfg.verbose:
            super().show()

class Test_Scheduler(unittest.TestCase):

    def test_scheduler_put(self):
        h = ob.hdl.Handler()
        h.walk("obot.shw")
        h.start()
        e = Event()
        e.etype = "command"
        e.orig = repr(h)
        e.origin = "root@shell"
        e.txt = "v"
        e.verbose = ob.cfg.verbose
        h.put(e)
        e.wait()
        self.assertTrue(e.result and "TEST" in e.result[0])
