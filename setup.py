# OB - make the machine ob, not the human. no copyright. no LICENSE.
#
# setup.py

from setuptools import setup, find_namespace_packages

setup(
    name='ob',
    version='30',
    url='https://bitbucket.org/botd/ob',
    author='Bart Thate',
    author_email='bthate@dds.nl',
    description="OB - make the machine ob, not the human. no copyright. no LICENSE.",
    long_description="""R E A D M E
###########

OB is a python3 "program your own commands" bot. no copyright. no LICENSE.

I N S T A L L


download the tarball from pypi, https://pypi.org/project/ob/#files


you can also download with pip3 and install globally.

::

 > sudo pip3 install ob --upgrade

if you want to develop on the library clone the source at bitbucket.org:

::

 > git clone https://bitbucket.org/botd/ob
 > cd ob
 > sudo python3 setup.py install


M O D U L E S


OB contains the following modules:

::

    ob                          - object library.
    ob.clk                      - clock functions.
    ob.dbs                      - database.
    ob.evt                      - basic event.
    ob.gnr                      - generic object functions.
    ob.hdl                      - handler.
    ob.ldr                      - module loader.
    ob.shl                      - shell code.
    ob.tms                      - time related functions.
    ob.thr                      - threads.
    ob.typ                      - typing.
    obot.cfg			- config command.
    obot.cmd			- list of commands.
    obot.fnd			- find objects.
    obot.irc			- irc bot.
    obot.rss			- feed fetcher.
    obot.shw			- show runtime.
    obot.udp			- udp to irc relay
    obot.usr			- user management.
    
have fun coding ;]


I N F O


you can contact me on IRC/freenode/#dunkbots.

| Bart Thate (bthate@dds.nl, thatebart@gmail.com)
| botfather on #dunkbots irc.freenode.net
    
    """,
    long_description_content_type="text/x-rst",
    license='Public Domain',
    zip_safe=False,
    install_requires=["feedparser"],
    packages=["ob", "obot"],
    scripts=["bin/ob", "bin/obot", "bin/oudp"],
    classifiers=['Development Status :: 3 - Alpha',
                 'License :: Public Domain',
                 'Operating System :: Unix',
                 'Programming Language :: Python',
                 'Topic :: Utilities'
                ]
)
