# OB - make the machine ob, not the human. no copyright. no LICENSE.
#
# event handler.

import ob
import logging
import queue
import time

def __dir__():
    return ("Handler",)

class Handler(ob.ldr.Loader):
 
    def __init__(self):
        super().__init__()
        self._queue = queue.Queue()
        self._stopped = False
        self.cbs = ob.Object()
        ob.Plain.objs.append(self)

    def handle_cb(self, event):
        if event.etype in self.cbs:
            self.cbs[event.etype](self, event)
        event.ready()

    def handler(self):
        while not self._stopped:
            e = self._queue.get()
            if e == None:
                break
            self.handle_cb(e)

    def poll(self):
        raise ENOTIMPLEMENTED

    def put(self, event):
        self._queue.put_nowait(event)

    def register(self, cbname, handler):
        logging.debug("register %s" % cbname)
        self.cbs[cbname] = handler        

    def start(self):
        self.register("command", dispatch)
        ob.thr.launch(self.handler)

    def stop(self):
        self._stopped = True
        self._queue.put(None)

    def wait(self):
        while not self._stopped:
            time.sleep(1.0)

def dispatch(handler, event):
    if not event.txt:
        event.ready()
        return
    event.parse()
    if "_func" not in event:
        chk = event.txt.split()[0]
        event._func = handler.cmds.get(chk, None)
    if event._func:
        event._func(event)
        event.show()
    event.ready()
    del event
