# OB - make the machine ob, not the human. no copyright. no LICENSE.
#
# databases. 

import ob
import os
import time
import _thread

def __dir__():
    return ("Db", "hook", "lock", "names")

lock = _thread.allocate_lock()

class ENOFILE(Exception):
    pass

class Db(ob.Object):

    def all(self, otype, selector={}, index=None, delta=0):
        nr = -1
        res = []
        for fn in names(otype, delta):
            o = hook(fn)
            nr += 1
            if index is not None and nr != index:
                continue
            if selector and not o.search(selector):
                continue
            if "_deleted" in o and o._deleted:
                continue
            res.append(o)
        return res

    def deleted(self, otype, selector={}):
        nr = -1
        res = []
        for fn in names(otype):
            o = hook(fn)
            nr += 1
            if selector and not o.search(selector):
                continue
            if "_deleted" not in o or not o._deleted:
                continue
            res.append(o)
        return res
        
    def find(self, otype, selector={}, index=None, delta=0):
        nr = -1
        res = []
        for fn in names(otype, delta):
            o = hook(fn)
            if o.search(selector):
                nr += 1
                if index is not None and nr != index:
                    continue
                if "_deleted" in o and o._deleted:
                    continue
                res.append(o)
        return res

    def find_value(self, otype, value, index=None, delta=0):
        nr = -1
        res = []
        for fn in names(otype, delta):
            o = hook(fn)
            for k, v in o.items():
                    if value in v:
                        nr += 1
                        if index is not None and nr != index:
                            continue
                        if "_deleted" in o and o._deleted:
                            continue
                        res.append(o)
        return res

    def last(self, otype, index=None, delta=0):
        fns = names(otype, delta)
        if fns:
            fn = fns[-1]
            return hook(fn)

    def last_all(self, otype, selector={}, index=None, delta=0):
        nr = -1
        res = []
        for fn in names(otype, delta):
            o = hook(fn)
            if selector and o.search(selector):
                nr += 1
                if index is not None and nr != index:
                    continue
                res.append((fn, o))
            else:
                res.append((fn, o))
        if res:
            s = sorted(res, key=lambda x: ob.fntime(x[0]))
            if s:
                return s[-1][-1]
        return None

@ob.locked(lock)
def hook(fn):
    t = fn.split(os.sep)[0]
    if not t:
        t = fn.split(os.sep)[0][1:]
    if not t:
        raise ENOFILE(fn)
    o = ob.typ.get_cls(t)()
    o.load(fn)
    return o

def names(name, delta=None):
    assert ob.workdir
    if not name:
        return []
    p = os.path.join(ob.workdir, "store", name) + os.sep
    res = []
    now = time.time()
    if delta:
        past = now + delta
    for rootdir, dirs, files in os.walk(p, topdown=True):
        for fn in files:
            fnn = os.path.join(rootdir, fn).split(os.path.join(ob.workdir, "store"))[-1]
            if delta:
                if ob.tms.fntime(fnn) < past:
                    continue
            res.append(os.sep.join(fnn.split(os.sep)[1:]))
    return sorted(res, key=ob.tms.fntime)
