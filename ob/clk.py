# OB - make the machine ob, not the human. no copyright. no LICENSE.
#
# clock module providing timers and repeaters 

import ob
import threading
import time
import typing

from ob.thr import launch
from ob.typ import get_name

def __dir__():
    return ("Repeater", "Timer", "Timers")

class Timer(ob.Object):

    def __init__(self, sleep, func, *args, **kwargs):
        super().__init__()
        self.func = func
        self.sleep = sleep
        self.args = args
        self.name = kwargs.get("name", "")
        self.kwargs = kwargs
        self.state = ob.Object()
        self.timer = None

    def run(self, *args, **kwargs):
        self.state.latest = time.time()
        ob.thr.launch(self.func, *self.args, **self.kwargs)

    def start(self):
        if not self.name:
            self.name = get_name(self.func)
        timer = threading.Timer(self.sleep, self.run, self.args, self.kwargs)
        timer.setName(self.name)
        timer.sleep = self.sleep
        timer.state = self.state
        timer.state.starttime = time.time()
        timer.state.latest = time.time()
        timer.func = self.func
        timer.start()
        self.timer = timer
        return timer

    def stop(self):
        if self.timer:
            self.timer.cancel()

class Repeater(Timer):

    def run(self, *args, **kwargs):
        thr = ob.thr.launch(self.start)
        self.func(*args, **kwargs)
        return thr
