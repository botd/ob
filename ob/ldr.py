# OB - make the machine ob, not the human. no copyright. no LICENSE.
#
# module loader.

import cmd
import importlib
import inspect
import ob
import logging
import os
import sys
import types

def __dir__():
    return ("Loader",)

class EINIT(Exception):

    pass

class ENOMODULE(Exception):

    pass

class Loader(ob.Object):

    table = ob.Object()

    def __init__(self):
        super().__init__()
        self.cmds = ob.Object()
        self.error = ""
        
    def direct(self, name):
        return importlib.import_module(name)

    def find_cmds(self, mod):
        c = ob.Object()
        for key, o in inspect.getmembers(mod, inspect.isfunction):
            if "event" in o.__code__.co_varnames:
                if o.__code__.co_argcount == 1:
                    c[key] = o
        return c

    def load_mod(self, mn, force=False, init=True):
        if mn in Loader.table:
            logging.warning("cache %s" % mn)
            return Loader.table[mn]
        logging.warning("load %s" % mn)
        mod = None
        if mn in sys.modules:
            mod = sys.modules[mn]
        else:
            try:
                mod = self.direct(mn)
            except ModuleNotFoundError as ex:
                if not mn in str(ex):
                    raise
                return
        if force or mn not in Loader.table:
            Loader.table[mn] = mod
        cmds = self.find_cmds(mod)
        self.cmds.update(cmds)
        if init and "init" in dir(mod):
            ob.thr.launch(mod.init,self)
        return Loader.table[mn]

    def walk(self, mns, init=False):
        if not mns:
            return
        mods = []
        for mn in mns.split(","):
            if not mn:
                continue
            if "." not in mn:
                mn = "obot.%s" % mn
            m = self.load_mod(mn, False)
            if not m:
                continue
            loc = None
            if "__spec__" in dir(m):
                loc = m.__spec__.submodule_search_locations
            if not loc:
                mods.append(m)
                continue
            for md in loc:
                for x in os.listdir(md):
                    if x.endswith(".py"):
                        mmn = "%s.%s" % (mn, x[:-3])
                        m = self.load_mod(mmn, True)
                        if m:
                            mods.append(m)
        return mods
