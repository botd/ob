# OB - make the machine ob, not the human. no copyright. no LICENSE.
#
# threading.

import ob
import logging
import queue
import threading
import types

def __dir__():
    return ("Launcher", "Thr", "launch")

class Thr(threading.Thread):

    def __init__(self, func, *args, name="noname", daemon=True):
        super().__init__(None, self.run, name, (), {}, daemon=daemon)
        self._name = name
        self._result = None
        self._queue = queue.Queue()
        self._queue.put((func, args))

    def __iter__(self):
        return self

    def __next__(self):
        for k in dir(self):
            yield k

    def run(self):
        func, args = self._queue.get()
        self._result = func(*args)

    def join(self, timeout=None):
        super().join(timeout)
        return self._result

class Launcher:

    def __init__(self):
        super().__init__()
        self._queue = queue.Queue()
        self._stopped = False

    def launch(self, func, *args, **kwargs):
        logging.debug("launch %s" % ob.typ.get_name(func))
        name = ""
        try:
            name = kwargs.get("name", args[0].name or args[0].txt)
        except (AttributeError, IndexError):
            name = ob.typ.get_name(func)
        t = Thr(func, *args, name=name)
        t.start()
        return t

def launch(func, *args):
    l = Launcher()
    return l.launch(func, *args)

def kill(thrname):
    for task in threading.enumerate():
        if thrname not in str(task):
            continue
        if "cancel" in dir(task):
            task.cancel()
        if "exit" in dir(task):
            task.exit()
        if "stop" in dir(task):
            task.stop()

